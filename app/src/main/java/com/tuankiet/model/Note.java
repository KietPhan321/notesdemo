package com.tuankiet.model;

import java.io.Serializable;

/**
 * Created by TuanKiet on 1/19/18.
 */

public class Note implements Serializable {
    private String id;
    private  String title;
    private String content;

    public Note(){

    }

    public Note (String title,String content){
        this.title = title;
        this.content = content;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
