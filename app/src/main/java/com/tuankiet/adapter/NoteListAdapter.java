package com.tuankiet.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.tuankiet.app.R;
import com.tuankiet.model.Note;

import java.util.ArrayList;
import java.util.List;

public class NoteListAdapter extends BaseAdapter {

    private  List<Note> objects = new ArrayList<>();

    private Context context;
    private LayoutInflater layoutInflater;

    public NoteListAdapter(Context context) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }


    public void setData(List<Note> data){

        if(data!=null){
            objects = data;
        }

        notifyDataSetChanged();

    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Note getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.note_list_item, null);
            convertView.setTag(new ViewHolder(convertView));
        }
        initializeViews((Note)getItem(position), (ViewHolder) convertView.getTag());
        return convertView;
    }

    private void initializeViews(Note object, ViewHolder holder) {

        holder.tvItemTitle.setText(object.getTitle());
    }

    protected class ViewHolder {
        private TextView tvItemTitle;;

        public ViewHolder(View view) {
            tvItemTitle = view.findViewById(R.id.tvItemTitle);
        }
    }
}
