package com.tuankiet.app;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tuankiet.database.FirebaseHelper;
import com.tuankiet.model.Note;


public class NoteActivity extends BaseActivity {


    private Button btnAdd;
    private Button btnDelete;
    private Button btnEdit;
    private EditText edtTitle;
    private EditText edtContent;

    private int index;
    private Note note;
    private boolean isAddMode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.note_layout);

        initView();

        initValue();
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = edtTitle.getText().toString();
                String content = edtContent.getText().toString();
                FirebaseHelper.getInstance().addNote(new Note(title, content));

                Toast.makeText(getApplicationContext(),getResources().getString(R.string.added), Toast.LENGTH_LONG).show();
                finish();
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = edtTitle.getText().toString();
                String content = edtContent.getText().toString();

                note.setTitle(title);
                note.setContent(content);

                FirebaseHelper.getInstance().editNote(note, index);
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.edited), Toast.LENGTH_LONG).show();

                finish();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(NoteActivity.this);
                builder.setMessage("Are you sure ?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                FirebaseHelper.getInstance().deleteNote(index);
                                Toast.makeText(getApplicationContext(),getResources().getString(R.string.deleted), Toast.LENGTH_LONG).show();
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        }).show();;
            }
        });
    }

    public void initView() {
        btnAdd = findViewById(R.id.btnAdd);
        btnDelete = findViewById(R.id.btnDelete);
        btnEdit = findViewById(R.id.btnEdit);

        edtTitle = findViewById(R.id.edtTitle);
        edtContent = findViewById(R.id.edtContent);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


    private void initValue() {

        Intent intent = getIntent();

        if (intent != null) {

            isAddMode = intent.getBooleanExtra(KEY.ADD_MODE, false);
            if (!isAddMode) {
                // Get position of selected note in list
                index = intent.getIntExtra(KEY.DATA, -1);
                if (index != -1) {
                    note = FirebaseHelper.getInstance().mNoteList.get(index);

                    if (note != null) {
                        edtTitle.setText(note.getTitle());
                        edtContent.setText(note.getContent());
                        //Disable add button in edit mode
                        btnDelete.setVisibility(View.VISIBLE);
                        btnEdit.setVisibility(View.VISIBLE);
                        btnAdd.setVisibility(View.GONE);
                    }
                }
            } else {
                //Disable edit button and delete button in add mode
                btnDelete.setVisibility(View.GONE);
                btnEdit.setVisibility(View.GONE);
                btnAdd.setVisibility(View.VISIBLE);
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
