package com.tuankiet.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.tuankiet.adapter.NoteListAdapter;
import com.tuankiet.database.FirebaseHelper;


public class MainActivity extends BaseActivity implements FirebaseHelper.FireBaseDBListener {

    private NoteListAdapter mAdapter;
    private ListView lvNote;
    private Intent mIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

        initValue();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIntent = new Intent(MainActivity.this,NoteActivity.class);
                mIntent.putExtra(KEY.ADD_MODE,true);
                startActivity(mIntent);
            }
        });

        lvNote.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                mIntent = new Intent(MainActivity.this,NoteActivity.class);
                mIntent.putExtra(KEY.ADD_MODE,false);
                mIntent.putExtra(KEY.DATA,position);

                startActivity(mIntent);
            }
        });
    }

    public void initView(){
        lvNote = findViewById(R.id.lvNoteList);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void initValue(){

        mAdapter = new NoteListAdapter(this);
        lvNote.setAdapter(mAdapter);

    }

    @Override
    protected void onResume() {
        super.onResume();

        FirebaseHelper.getInstance().getDataBase(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSync() {
        mAdapter.setData(FirebaseHelper.getInstance().mNoteList);
    }
}
