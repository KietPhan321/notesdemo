package com.tuankiet.database;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tuankiet.app.KEY;
import com.tuankiet.model.Note;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class FirebaseHelper {

    private final String TAG = FirebaseHelper.class.getSimpleName();

    private static FirebaseHelper sFirebaseHelper;

    private DatabaseReference db;

    public List<Note> mNoteList;




    public static FirebaseHelper getInstance() {

        if (sFirebaseHelper == null) sFirebaseHelper = new FirebaseHelper();
        return sFirebaseHelper;

    }

    private FirebaseHelper() {
        //Enable support offline mode
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        this.db = FirebaseDatabase.getInstance().getReference();

        mNoteList = new ArrayList<>();
    }


    public void addNote(Note note) {

        if (db == null || note == null) return;
        mNoteList.add(note);

        db.child(KEY.NOTES_NODE).setValue(mNoteList);

    }


    public void editNote(Note note, int index) {

        if (db == null || note == null) return;

        mNoteList.set(index,note);

        db.child(KEY.NOTES_NODE).setValue(mNoteList);

    }


    public void deleteNote(int index) {

        if (db == null && index !=-1) return;

        mNoteList.remove(index);

        db.child(KEY.NOTES_NODE).setValue(mNoteList);

    }


    public void getDataBase(final FireBaseDBListener listener) {

        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                mNoteList.clear();

                Log.d(TAG, dataSnapshot.toString());

                Map<String, Object> objectMap = (Map<String, Object>) dataSnapshot.getValue();

                if (objectMap != null) {

                    List<Map<String,String>> list = (List<Map<String, String>>) objectMap.get(KEY.NOTES_NODE);

                  for(Map<String,String> tmp : list){

                      String title = tmp.get("title");
                      String content = tmp.get("content");

                      Note note = new Note(title, content);

                      mNoteList.add(note);
                  }


                    if(listener!=null){
                        listener.onSync();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "onCancelled", databaseError.toException());
            }
        };

        db.addValueEventListener(postListener);

    }


    public interface FireBaseDBListener{

        void onSync();
    }


}
